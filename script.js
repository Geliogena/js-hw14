/*
Теоретичні питання.
1. Об'єкти веб-сховища браузера "localStorage" та "sessionStorage" зберігають дані у вигляді (ключ/значення), не надсилаючи
їх до сервера. Відмінність в тім, що дані для "sessionStorage" зберігаються доки триває сесія сторінки, навіть при її оновленні.
А дані для "localStorage" зберігаються навіть при повному закриттю, та новому відкриттю сторінки.
2. Веб - сховище прив’язане до оригінального сайту (домен/протокол/порт) таким чином, що різні протоколи або субдомени мають
різні об’єкти зберігання, і не можуть отримати доступ до даних один одного. При використанні Local Storage важливо дотримуватись
 політики одного джерела та використовувати HTTPS для захищеної передачі інформації. Все ж таки рекомендується уникати збереження
 конфіденційної інформації в Local Storage через ризик XSS-атак (англ. cross-Site scripting - «міжсайтовий скриптинг»).
3. Дані зі сховища "sessionStorage" видаляються при завершенні сеанса браузера.
*/
"use strict"
const div = document.querySelector(".main__learn-description");
const newBtn = document.createElement("button");
newBtn.textContent = "Змінити тему";
newBtn.id = "change";
div.prepend(newBtn);
const head = document.querySelector("head");
const newLink = document.createElement("link");
newLink.innerHTML = '<link rel = stylesheet href = "../styles/dark-background.css">';
newLink.id = "dark";
const button = document.getElementById("change");
button.classList.toggle("active");
button.addEventListener("click", function(e){
    const active = document.querySelector(".active");
    if (e.target === active){
        head.append(newLink);
        sessionStorage.setItem("theme", "dark");
        button.classList.toggle("active");
    } else{
        document.getElementById("dark").remove();
        sessionStorage.removeItem("theme");
        button.classList.toggle("active");
    }
});
document.addEventListener("DOMContentLoaded", () => {
    if(sessionStorage.getItem("theme") === "dark"){
        head.append(newLink);
        button.classList.toggle("active");
    }
});
